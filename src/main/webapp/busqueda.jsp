<%-- 
    Document   : principal
    Created on : 08-04-2021, 19:45:15
    Author     : Ripley
--%>

<%@page import="modelo.pronosticos.Pronostico"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="util.Menu"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Pronostico> pronosticos = (List<Pronostico>) request.getAttribute("listaPronosticos");
    Iterator<Pronostico> itPronosticos = pronosticos.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>

</style>
</head>
<body>

     <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
                <table border="1">
                    <thead>
                    <th>Dia</th>
                    <th>Condicion </th>
                    <th>Temperatura </th>
                     <th>Viento </th>
                    </thead>
                    <tbody>
                        <%while (itPronosticos.hasNext()) {
                       Pronostico pro = itPronosticos.next();%>
                        <tr>
                            <td><%= pro.getDia()%></td>
                            <td><%= pro.getCondicion()%></td>
                            <td><%= pro.getTemperatura()%></td>
                            <td><%= pro.getViento()%></td>
                           
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
    
    
  
  

</body>
</html>
