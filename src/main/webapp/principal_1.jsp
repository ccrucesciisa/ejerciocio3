<%-- 
    Document   : principal
    Created on : 08-04-2021, 19:45:15
    Author     : Ripley
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="util.Menu"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Menu> menus = (List<Menu>) request.getAttribute("listaMenus");
    Iterator<Menu> itJmenus = menus.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>JSP Page</title>
    </head>
    <style>
.mainmenubtn {
    background-color: skyblue;
    color: white;
    border: none;
    cursor: pointer;
    padding:20px;
    margin-top:20px;
}
.mainmenubtn:hover {
    background-color: blue;
    }
.dropdown {
    position: relative;
    display: inline-block;
}
.dropdown-child {
    display: none;
    background-color: skyblue;
    min-width: 200px;
}
.dropdown-child a {
    color: blue;
    padding: 20px;
    text-decoration: none;
    display: block;
}
.dropdown:hover .dropdown-child {
    display: block;
}
</style>
</head>
<body>
<div class="dropdown">
  <button class="mainmenubtn">Main Menu</button>
  <div class="dropdown-child">
   
       <%while (itJmenus.hasNext()) {
                       Menu menu = itJmenus.next();%>
                       
                       
      <a href="<%= menu.getUrl()%>" target="_blank"><%= menu.getLabel()%></a>
 <%}%>    
  </div>
</div>
  
  

</body>
</html>
