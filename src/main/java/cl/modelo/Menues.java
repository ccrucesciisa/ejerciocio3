/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;

/**
 *
 * @author ccruces
 */
public class Menues {

    private String codigo;
    private String descripcion;
    private String descripcionp2;

    private String precio;

    /**
     * @return the codigo
     */
    /**
     * @return the descripcionp2
     */
    public String getDescripcionp2() {
        return descripcionp2;
    }

    /**
     * @param descripcionp2 the descripcionp2 to set
     */
    public void setDescripcionp2(String descripcionp2) {
        this.descripcionp2 = descripcionp2;
    }

    public Menues(String codigo, String descripcion, String precio, String descripcionp2) {

        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.descripcionp2=descripcionp2;

    }

    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the precio
     */
    public String getPrecio() {
        return precio;
    }

    /**
     * @param precio the precio to set
     */
    public void setPrecio(String precio) {
        this.precio = precio;
    }

}
