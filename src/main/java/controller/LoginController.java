/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import cl.modelo.Menues;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.pronosticos.Pronostico;
import util.Autenticador;
import util.Menu;

/**
 *
 * @author ccruces
 */
@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("Uname");
        String password = request.getParameter("Pass");

        System.out.println("el usuario es :" + username + ", el password es:" + password);
        Autenticador aut = new Autenticador();

        //  if (aut.validaLogin(username, password)) {
        if (true) {
            /*  List<Menu> listaMenus=new ArrayList<Menu>();
            Menu m1=new Menu();
            m1.setLabel("opcion menu 1");
            m1.setUrl("www.google.com");
            
            listaMenus.add(m1);
            
            Menu m2=new Menu();
            m2.setLabel("opcion menu 2");
            m2.setUrl("www.sii.cl");
            
            listaMenus.add(m2);
            
            Menu m3=new Menu();
            m3.setLabel("opcion menu 3");
            m3.setUrl("www.latercera.cl");
            
            listaMenus.add(m3);
           
            
            request.setAttribute("listaMenus", listaMenus);*/

            List<Menues> listaMenues = new ArrayList<Menues>();

            Menues m1 = new Menues("1", "Salmon ", "7500","con Papas Fritas");
            Menues m2 = new Menues("1", "Arroz ", "4500","con Pollo");
            Menues m3 = new Menues("1", "Bife ", "9800","a lo pobre ");
            Menues m4 = new Menues("1", "Fideos  ", "4500", "con salsa boloñesa");
            listaMenues.add(m1);
            listaMenues.add(m2);
            listaMenues.add(m3);
            listaMenues.add(m4);
            request.setAttribute("listaMenues", listaMenues);
            
            
            List<Menues> listaMenuesMar = new ArrayList<Menues>();

            Menues m1mar = new Menues("1", "pescado Frito ", "7500","con Papas Fritas");
            Menues m2mar = new Menues("1", "Salmon ", "4500","con Arroz");
            Menues m3mar = new Menues("1", "Pescado a la olla ", "9800"," con papas ");
            Menues m4mar = new Menues("1", "Fideos  ", "4500", "con salsa boloñesa");
            listaMenuesMar.add(m1mar);
            listaMenuesMar.add(m2mar);
            listaMenuesMar.add(m3mar);
            listaMenuesMar.add(m4mar);
            request.setAttribute("listaMenuesMar", listaMenuesMar);
            
            request.getRequestDispatcher("principal.jsp").forward(request, response);
           
            
            
            
            /*
            List<Pronostico> listaPronosticos = new ArrayList<Pronostico>();

            Pronostico p1 = new Pronostico();
            p1.setDia("hoy");
            p1.setCondicion("Soleado");
            p1.setTemperatura("19°C");
            p1.setViento("E 13 km/h");
            listaPronosticos.add(p1);

            Pronostico p2 = new Pronostico();
            p2.setDia("mañana");
            p2.setCondicion("Nublado");
            p2.setTemperatura("19°C");
            p2.setViento("E 13 km/h");
            listaPronosticos.add(p2);

            Pronostico p3 = new Pronostico();
            p3.setDia("Sabado");
            p3.setCondicion("Parcialmente Nublado");
            p3.setTemperatura("19°C");
            p3.setViento("E 13 km/h");
            listaPronosticos.add(p3);

              request.setAttribute("listaPronosticos", listaPronosticos);
            request.getRequestDispatcher("busqueda.jsp").forward(request, response);*/
        } else {
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
